import React from 'react';
import * as PreloaderData from "react-preloader-icon";

const {Preloader} = PreloaderData;
const PreloaderTypes = [];
Object.keys(PreloaderData).forEach((key) => {
   PreloaderTypes.push(key);
})
PreloaderTypes.splice(PreloaderTypes.indexOf('Preloader'), 1)

const BackdropPreloader = ({option}) => {
   if (!option) option = {};
   if (option.toString() !== "[object Object]") option = {};
   let {type} = option;
   if (type && type !== 'Preloader') {
      if (typeof parseInt(type) === 'number' && type < PreloaderTypes.length) {
         if (PreloaderTypes[type]) {
            type = PreloaderTypes[type];
         }
      } else if (!PreloaderTypes.find(t => t === type)) {
         type = PreloaderTypes[1];
      }
   } else {
      type = PreloaderTypes[0];
   }

   return (
     <div style={
        {
           position: 'fixed',
           left: '0',
           top: '0',
           display: 'flex',
           zIndex: '1000',
           background: 'rgba(239,238,238,0.7)',
           width: '100%',
           height: '100vh',
        }}>
        <div style={{margin: 'auto'}}>
           <Preloader
             use={PreloaderData[type]}
             size={132}
             strokeWidth={10}
             strokeColor="black"
             duration={1200}
           />
        </div>
     </div>
   );
};

export default BackdropPreloader;