import Counter from "./containers/Counter/Counter";
import ToDoList from "./containers/ToDoList/ToDoList";

const App = () => {
  console.log('in App');

  return (
    <>
    <Counter/>
    <ToDoList/>
    </>
  );
};

export default App;
