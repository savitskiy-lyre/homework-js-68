import axios from 'axios';

//export const COUNTER_CHANGERS = 'COUNTER_CHANGERS';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';
export const SEND_TASKS_REQUEST = 'SEND_TASKS_REQUEST';
export const SEND_TASKS_SUCCESS = 'SEND_TASKS_SUCCESS';
export const SEND_TASKS_FAILURE = 'SEND_TASKS_FAILURE';

export const increase = () => ({type: INCREASE, /*group: COUNTER_CHANGERS*/});
export const decrease = () => ({type: DECREASE, /*group: COUNTER_CHANGERS*/});
export const add = value => ({type: ADD, payload: value, /*group: COUNTER_CHANGERS*/});
export const subtract = value => ({type: SUBTRACT, payload: value, /*group: COUNTER_CHANGERS*/});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = (counter) => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});
export const sendTasksRequest = (tasks) => ({type: SEND_TASKS_REQUEST, payload: tasks});
export const sendTasksSuccess = () => ({type: SEND_TASKS_SUCCESS});
export const sendTasksFailure = (error) => ({type: SEND_TASKS_FAILURE, payload: error});

export const fetchCounter = () => {
   return async (dispatch) => {
      dispatch(fetchCounterRequest());

      try {
         const response = await axios.get('https://burgerjs1011-default-rtdb.firebaseio.com/counter.json');

         if (response.data === null) {
            dispatch(fetchCounterSuccess(0));
         } else {
            dispatch(fetchCounterSuccess(response.data));
         }
      } catch (e) {
         dispatch(fetchCounterFailure());
      }
   };
};

export const sendTasks = (tasks) => {
   return async (dispatch) => {
      dispatch(sendTasksRequest(tasks));
      try {
         const response = await axios.post('https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/h68/tasks.json', {tasks: tasks, time: new Date()});
         if (response.data) dispatch(sendTasksSuccess());
      } catch (e) {
         dispatch(sendTasksFailure(e));
      }
   };
};

export const saveCounter = () => {
   return async (dispatch, getState) => {
      const currentCounter = getState().counter;
      await axios.post('https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/h68/counterWasChanged.json', {
         counter: currentCounter,
         time: new Date()
      });
   };
}
