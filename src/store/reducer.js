import {
   ADD,
   DECREASE,
   FETCH_COUNTER_FAILURE,
   FETCH_COUNTER_REQUEST,
   FETCH_COUNTER_SUCCESS,
   INCREASE, SEND_TASKS_FAILURE, SEND_TASKS_REQUEST, SEND_TASKS_SUCCESS,
   SUBTRACT,
} from "./actions";
//import axios from "axios";

const initialState = {
   counter: 123,
   loading: false,
   error: null,
};


const add = (state, action) => {
   return {...state, counter: state.counter + action.payload};
}

const reducer = (state = initialState, action) => {
   switch (action.type) {
      case INCREASE:
         return {...state, counter: state.counter + 1};
      case DECREASE:
         return {...state, counter: state.counter - 1};
      case ADD:
         return add(state, action);
      case SUBTRACT:
         return {...state, counter: state.counter - action.payload};
      case FETCH_COUNTER_REQUEST:
         return {...state, error: null, loading: true};
      case FETCH_COUNTER_SUCCESS:
         return {...state, loading: false, counter: action.payload};
      case FETCH_COUNTER_FAILURE:
         return {...state, loading: false, error: action.payload};
      case SEND_TASKS_REQUEST:
         return {...state, error: null, loading: true};
      case SEND_TASKS_SUCCESS:
         //alert('SUCCESS !!!');
         return {...state, loading: false};
      case SEND_TASKS_FAILURE:
         //alert(action.payload);
         return {...state, loading: false, error: action.payload};
      default:
         return state;
   }
}

/* Можно ли как то сделать группу акшинов ?
Например как то сгруппировать акшины и делать что то общее, вроде того что накалякано ниже:

const saveCounter = async (counter) => {
   const response = await axios.post('https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/h68/counterWasChanged.json', {counter: counter, time: new Date()});
   console.log(response);
};

const add = (nextState, action) => {
   nextState.counter = nextState.counter + action.payload;
}

const reducer = (state = initialState, action) => {
   if (action.type){
      const nextState = {...state};

      switch (action.type) {
         case INCREASE:
            nextState.counter = nextState.counter + 1;
            break;
         case DECREASE:
            nextState.counter = nextState.counter - 1;
            break;
         case ADD:
            add(nextState, action)
            //nextState.counter = nextState.counter + action.payload;
            break;
         case SUBTRACT:
            nextState.counter = nextState.counter - action.payload;
            break;
         case FETCH_COUNTER_REQUEST:
            nextState.error = null;
            nextState.loading = true;
            break;
         case FETCH_COUNTER_SUCCESS:
            nextState.loading = false;
            nextState.counter = action.payload;
            break;
         case FETCH_COUNTER_FAILURE:
            nextState.loading = false;
            nextState.error = action.payload;
            break;
         default:
            return state;
      }

      switch (action.group) {
         case COUNTER_CHANGERS:
            saveCounter(nextState.counter)
            break;
      }

      return nextState;
   }

}*/

export default reducer;